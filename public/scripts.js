
// const socket = io('http://localhost:9000'); // the / namespace/endpoint
const socket1 = io('http://localhost:9000')
//const socket2 = io('http://localhost:9000/wiki')
//const socket3 = io('http://localhost:9000/mozilla')
//const socket4 = io('http://localhost:9000/linux')


//Listen for nsList, which is a list of all the namespaces
socket1.on('nsList', nsData => {
    let namespacesDiv = document.querySelector('.namespaces')
    namespacesDiv.innerHTML = ""
    nsData.forEach((ns)=>{
        namespacesDiv.innerHTML += `<div class="namespace" ns=${ns.endpoint} ><img src="${ns.img}" /></div>`
    })
    Array.from(document.getElementsByClassName('namespace')).forEach((elem)=>{
        // console.log(elem)
        elem.addEventListener('click',(e)=>{
            const nsEndpoint = elem.getAttribute('ns');
             console.log(`${nsEndpoint} I should go to now`)
        })
    })
})